
# Marmellata Project
Author: Leonardo Riva


### Description
Kubernetes project 


### Requirements
- Minikube 1.29.0
- VirtualBox 7.0


### Procedure

- Open a terminal inside the working directory. 

- Create a Dockerfile

- Build an image: `docker build -t marmellata:1.0 .`

- Start Minikube (Kubernetes): `minikube start --vm-driver=virtualbox`

- Install Kubernetes metrics server

- Create a YAML file

- Run containers: `kubectl apply -f marmellata.yaml`

- Set up Autoscaler: `kubectl autoscale deployment marmellata-deployment --cpu-percent=50 --min=1 --max=10`

- Get ip address: `minikube service node-service`

